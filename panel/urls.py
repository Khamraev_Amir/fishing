from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('confirm/', views.confirm, name = 'confirm'),
    path('confirm/<int:phone_id>/', views.confirm_code, name = 'confirm_code'),
    path('finished/<int:phone_id>/', views.success, name = 'success'),
]