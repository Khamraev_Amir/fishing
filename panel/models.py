from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

 
# Shop
class Phone(models.Model):
    number = models.CharField('Номер телефона', max_length=150, null=True)
    code = models.CharField('Код', max_length=150, null=True)

    class Meta:
        verbose_name = 'Номер'
        verbose_name_plural = 'Номера'

    def __str__(self):
        return str(self.number) + ' | ' + str(self.code)   

